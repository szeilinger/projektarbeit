﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Input;
using Framework.Controllers;
using Remotion.Linq.Utilities;

namespace Framework.Various
{
    internal class OpenWindowCommand : ICommand
    {
        #region Fields

        private readonly Action<object, IController> _execute;
        private readonly Type _controllerType;

        private static List<Type> _instanciatedControllers;
        public static List<Type> InstanciatedControllers
        {
            get { return _instanciatedControllers ?? (_instanciatedControllers = new List<Type>()); }
        }

        #endregion // Fields

        #region Constructors

        public OpenWindowCommand(Type controllerType, Action<object, IController> execute)
        {
            if (controllerType == null)
                throw new ArgumentNullException("controllerType");
            if (!(typeof (IController).IsAssignableFrom(controllerType)))
                throw new ArgumentTypeException("Type has to implement IController", "controllerType",
                                                typeof (IController), controllerType);
            if (execute == null)
                throw new ArgumentNullException("execute");

            _execute = execute;
            _controllerType = controllerType;
        }

        #endregion // Constructors

        #region ICommand Members

        [DebuggerStepThrough]
        public bool CanExecute(object parameter)
        {
            return !InstanciatedControllers.Contains(_controllerType);
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            InstanciatedControllers.Add(_controllerType);
            _execute(parameter, (IController)Activator.CreateInstance(_controllerType));
        }

        #endregion // ICommand Members
    }
}
