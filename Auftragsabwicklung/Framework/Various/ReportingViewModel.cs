// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ReportingViewModel.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the ReportingViewModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using Framework.Models;

namespace Framework.Various
{
    class ReportingViewModel : ViewModelBase
    {
        private bool _byYear;
        private bool _byMonth;
        private bool _byManualInput;
        private bool _byArea;
        private DateTime _dateFrom;
        private DateTime _dateTo;
        private EMonth _month;
        private int _year;
        private string _area;

        public ReportingViewModel()
        {
            _dateFrom = DateTime.Today;
            _dateTo = DateTime.Today.AddYears(1);
        }
        public bool ByYear
        {
            get { return _byYear; }
            set
            {
                if (_byYear == value) return;
                _byYear = value;
                OnPropertyChanged("ByYear");
                OnPropertyChanged("Models");
            }
        }
        public bool ByMonth
        {
            get { return _byMonth; }
            set
            {
                if (_byMonth == value) return;
                _byMonth = value;
                OnPropertyChanged("ByMonth");
                OnPropertyChanged("Models");
            }
        }
        public bool ByManualInput
        {
            get { return _byManualInput; }
            set
            {
                if (_byManualInput == value) return;
                _byManualInput = value;
                OnPropertyChanged("ByManualInput");
                OnPropertyChanged("Models");
            }
        }
        public EMonth Month
        {
            get { return _month; }
            set
            {
                if (_month == value) return;
                _month = value;
                OnPropertyChanged("Month");
                OnPropertyChanged("Models");
            }
        }
        public int Year
        {
            get { return _year; }
            set
            {
                if (_year == value) return;
                _year = value;
                OnPropertyChanged("Year");
                OnPropertyChanged("Models");
            }
        }
        public DateTime DateFrom
        {
            get { return _dateFrom; }
            set
            {
                if (_dateFrom == value) return;
                _dateFrom = value;
                OnPropertyChanged("DateFrom");
                OnPropertyChanged("Models");
            }
        }
        public DateTime DateTo
        {
            get { return _dateTo; }
            set
            {
                if (_dateTo == value) return;
                _dateTo = value;
                OnPropertyChanged("DateTo");
                OnPropertyChanged("Models");
            }
        }

        public IEnumerable<string> Months
        {
            get { return from EMonth a in Enum.GetValues(typeof(EMonth)) select EnumDescriptionConverter.GetDescription(a); }
        }
        public IEnumerable<int> Years
        {
            get
            {
                var session = HybridSessionBuilder.OpenSession();
                var v = session.CreateSQLQuery(@"Select OrderDate from Orders").List<DateTime>();
                return v.Select(x => x.Year).Distinct();
            }
        }

        public IEnumerable<string> Areas
        {
            get
            {
                var session = HybridSessionBuilder.OpenSession();
                var v = session.CreateSQLQuery(@"SELECT Area FROM Employees GROUP BY Area").List<string>();
                return v;
            }
        }
        public string Area
        {
            get { return _area; }
            set
            {
                if (_area == value) return;
                _area = value;
                OnPropertyChanged("Area");
                OnPropertyChanged("Models");
            }
        }
        public bool ByArea
        {
            get { return _byArea; }
            set
            {
                if (_byArea == value)
                    return;
                _byArea = value;
                OnPropertyChanged("ByArea");
                OnPropertyChanged("Models");
            }
        }

        public DateTime DateSpanFrom
        {
            get
            {
                if (ByYear && Year > 0) return new DateTime(Year, 1, 1);
                if (ByMonth && Year > 0 && Month!=EMonth.NONE) return new DateTime(Year, (int)Month, 1);
                if (ByManualInput) return DateFrom;
                return default(DateTime);
            }
        }
        public DateTime DateSpanTo
        {
            get
            {
                if (ByYear && Year > 0) return new DateTime(Year + 1, 1, 1).AddMilliseconds(-1);
                if (ByMonth && Year > 0)
                {
                    if (Month == EMonth.DEZEMBER)
                    {
                        return new DateTime(Year + 1, 1, 1).AddMilliseconds(-1);
                    }
                    return new DateTime(Year, (int)Month + 1, 1).AddMilliseconds(-1);
                }
                if (ByManualInput) return DateTo.AddDays(1).AddMilliseconds(-1);
                return default(DateTime);
            }
        }
    }
}
