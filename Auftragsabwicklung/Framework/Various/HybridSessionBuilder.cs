﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HybridSessionBuilder.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the HybridSessionBuilder type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Diagnostics;
using System.Reflection;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;

namespace Framework.Various
{
    public class HybridSessionBuilder
    {
        private static ISession _currentSession;
        public static string DatabaseFile = @"Database\CompanyManagementSystem.db";

        private static HybridSessionBuilder _sessionBuilder;
        private static HybridSessionBuilder SessionBuilder
        {
            get
            {
                if(_sessionBuilder == null)
                    _sessionBuilder = new HybridSessionBuilder();

                return _sessionBuilder;
            }
        }

        private static ISessionFactory _sessionFactory;
        private ISessionFactory SessionFactory
        {
            get
            {
                if (_sessionFactory == null)
                {
                    _sessionFactory = Fluently.Configure()
                                              .Database(SQLiteConfiguration.Standard.UsingFile(DatabaseFile).ShowSql())
                                              .Mappings(
                                                  m => m.FluentMappings.AddFromAssembly(Assembly.GetExecutingAssembly())
                                                        .Conventions.Add(
                                                            FluentNHibernate.Conventions.Helpers.DefaultLazy.Never()))
                                              .BuildSessionFactory();
                }

                return _sessionFactory;
            }
        }

        public ISession GetSession()
        {
            ISessionFactory factory = SessionFactory;
            ISession session = getExistingOrNewSession(factory);
            Debug.WriteLine("Using ISession " + session.GetHashCode());
            return session;
        }

        public static ISession OpenSession()
        {
            return SessionBuilder.GetSession();
        }

        private ISession getExistingOrNewSession(ISessionFactory factory)
        {
            if (_currentSession == null)
            {
                _currentSession = factory.OpenSession();
            }
            else if (!_currentSession.IsOpen)
            {
                _currentSession = factory.OpenSession();
            }

            return _currentSession;
        }

        public static void ResetSession()
        {
            SessionBuilder.GetSession().Dispose();
        }

        public static void ChangeDatabase(string database)
        {
            DatabaseFile = database;
            SessionBuilder.GetSession().Dispose();
            _sessionFactory = null;
        }
    }
}
