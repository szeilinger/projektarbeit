﻿using System.Collections.Generic;
using NHibernate;

namespace Framework.Various
{
    public class Repository<T> where T : class
    {
        public List<T> GetAll()
        {
            var session = HybridSessionBuilder.OpenSession();
            var returnList = session.CreateCriteria<T>().List<T>();
            return returnList as List<T>;
        }

        public IQueryOver<T,T> Query()
        {
            var session = HybridSessionBuilder.OpenSession();
            return session.QueryOver<T>();
        }

        public void Delete(T entity)
        {
            var session = HybridSessionBuilder.OpenSession();
            using (var transaction = session.BeginTransaction())
            {
                session.Delete(entity);
                transaction.Commit();
            }
        }

        public void Save(T entity)
        {
            var session = HybridSessionBuilder.OpenSession();
            using (var transaction = session.BeginTransaction())
            {
                session.Save(entity);
                transaction.Commit();
            }
        }

        public void Update(T entity)
        {
            var session = HybridSessionBuilder.OpenSession();
            using (var transaction = session.BeginTransaction())
            {
                session.Update(entity);
                transaction.Commit();
            }
        }
    }
}
