// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StartItem.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the StartItem type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;

namespace Framework.Various
{
    class StartItem
    {
        public string Caption { get; private set; }
        public Type Controller { get; private set; }
        
        public StartItem(string caption, Type controller)
        {
            Caption = caption;
            Controller = controller;
        }
    }
}
