﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CountryAddViewModel.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the CountryAddViewModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Windows.Input;
using Framework.Models;
using Framework.Various;

namespace Framework.ViewModels
{
    class CountryAddViewModel : ViewModelBase
    {
        public Country Model { get; set; }
        public ICommand OkCommand { get; set; }
        public ICommand CancelCommand { get; set; }

        public string Name
        {
            get { return Model.Name; }
            set
            {
                if (Model.Name == value)
                    return;
                Model.Name = value;
                OnPropertyChanged("Name");
            }
        }

        public bool IsValid
        {
            get { return Model.IsValid; }
        }
    }
}
