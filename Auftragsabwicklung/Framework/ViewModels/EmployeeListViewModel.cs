﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EmployeeListViewModel.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the EmployeeListViewModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Windows.Input;
using Framework.Models;
using System.Collections.ObjectModel;
using Framework.Various;

namespace Framework.ViewModels
{
    class EmployeeListViewModel : ViewModelBase
    {
        private ObservableCollection<Employee> mModels;
        public ObservableCollection<Employee> Models
        {
            get { return mModels; }

            set
            {
                if (mModels == value)
                    return;
                mModels = value;
                OnPropertyChanged("Models");
            }
        }

        private Employee mSelectedModel;
        public Employee SelectedModel
        {
            get { return mSelectedModel; }
            set
            {
                if (mSelectedModel == value)
                    return;
                mSelectedModel = value;
                base.OnPropertyChanged("SelectedModel");
            }
        }

        public ICommand AddCommand { get; set; }
        public ICommand EditCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
    }
}