﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerListViewModel.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the CustomerListViewModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Windows.Input;
using Framework.Models;
using System.Collections.ObjectModel;
using Framework.Various;

namespace Framework.ViewModels
{
    class CustomerListViewModel : ViewModelBase
    {
        private ObservableCollection<Customer> mModels;
        public ObservableCollection<Customer> Models
        {
            get { return mModels; }

            set
            {
                if (mModels == value)
                    return;
                mModels = value;
                OnPropertyChanged("Models");
            }
        }

        private Customer mSelectedModel;
        public Customer SelectedModel
        {
            get { return mSelectedModel; }
            set
            {
                if (mSelectedModel == value)
                    return;
                mSelectedModel = value;
                base.OnPropertyChanged("SelectedModel");
            }
        }

        public ICommand AddCommand { get; set; }
        public ICommand EditCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
    }
}
