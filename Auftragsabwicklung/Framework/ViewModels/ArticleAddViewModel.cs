﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ArticleAddViewModel.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the ArticleAddViewModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Windows.Input;
using Framework.Models;
using Framework.Various;

namespace Framework.ViewModels
{
    internal class ArticleAddViewModel : ViewModelBase
    {
        public Article Model { get; set; }
        public ICommand OkCommand { get; set; }
        public ICommand CancelCommand { get; set; }
        public ICommand ImportCommand { get; set; }

        public string ArticleNumber
        {
            get { return Model.ArticleNumber; }
            set
            {
                if (Model.ArticleNumber == value)
                    return;
                Model.ArticleNumber = value;
                OnPropertyChanged("ArticleNumber");
            }
        }

        public string Name
        {
            get { return Model.Name; }
            set
            {
                if (Model.Name == value)
                    return;
                Model.Name = value;
                OnPropertyChanged("Name");
            }
        }

        public double Price
        {
            get { return Model.Price; }
            set
            {
                if (Model.Price == value)
                    return;
                Model.Price = value;
                OnPropertyChanged("Price");
            }
        }

        public string Description
        {
            get { return Model.Description; }
            set
            {
                if (Model.Description == value)
                    return;
                Model.Description = value;
                OnPropertyChanged("Description");
            }
        }

        public DateTime ValidFrom
        {
            get { return Model.ValidFrom; }
            set
            {
                if (Model.ValidFrom == value)
                    return;
                Model.ValidFrom = value;
                OnPropertyChanged("ValidFrom");
            }
        }

        public DateTime ValidTo
        {
            get { return Model.ValidTo; }
            set
            {
                if (Model.ValidTo == value)
                    return;
                Model.ValidTo = value;
                OnPropertyChanged("ValidTo");
            }
        }

        public bool IsValid
        {
            get
            {
                return Model.IsValid;
            }
        }
    }
}