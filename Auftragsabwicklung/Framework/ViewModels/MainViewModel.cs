﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Framework.Controllers;
using Framework.Models;
using Framework.Various;

namespace Framework.ViewModels
{
    internal class MainViewModel : ViewModelBase
    {
        private StackPanel _details;
        private StackPanel _stackPanel;
        public ICommand ChooseDatabaseCommand { get; set; }
        public ICommand CloseCommand { get; set; }
        public ICommand AboutCommand { get; set; }
        public ICommand MainWindowCommand { get; set; }

        public StackPanel Details
        {
            get
            {
                return _details ??
                       (_details =
                        new StackPanel());
            }
        }

        public StackPanel StackPanel
        {
            get { return _stackPanel ?? (_stackPanel = new StackPanel() {Orientation = Orientation.Horizontal}); }
        } 

        public void AddDetail(Control control)
        {
            Details.Children.Add(control);
            OnPropertyChanged("Details");
        }

        public void ClearDetails()
        {
            Details.Children.Clear();
            OnPropertyChanged("Details");
        }


        public IEnumerable<StartItem> MasterDetails
        {
            get
            {
                return new List<StartItem>()
                           {
                               new StartItem("Mitarbeiter", typeof (EmployeeListController)),
                               new StartItem("Kunden", typeof (CustomersListController)),
                               new StartItem("Artikel", typeof (ArticleListController))
                           };
            }
        }

        public IEnumerable<StartItem> ReportingDetails
        {
            get
            {
                return new List<StartItem>()
                           {
                               new StartItem("Mitarbeiter", typeof (EmployeeReportingController)),
                               new StartItem("Kunden", typeof (CustomerReportingController)),
                               new StartItem("Artikel", typeof (ArticleReportingController))
                           };
            }
        }

        public IEnumerable<StartItem> OrderDetails
        {
            get
            {
                return new List<StartItem>()
                           {
                               new StartItem("Verwaltung", typeof (OrderListController)),
                               new StartItem("Import", typeof (OrderImportListController))
                           };
            }
        }
    }
}
