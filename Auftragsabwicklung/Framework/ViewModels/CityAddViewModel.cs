﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CityAddViewModel.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the CityAddViewModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;
using System.Windows.Input;
using Framework.Models;
using Framework.Various;

namespace Framework.ViewModels
{
    internal class CityAddViewModel : ViewModelBase
    {
        public City Model { get; set; }
        public ICommand OkCommand { get; set; }
        public ICommand CancelCommand { get; set; }
        public ICommand AddCountryCommand { get; set; }

        public IList<Country> Countries
        {
            get { var resultList = new Repository<Country>().Query().List<Country>();
                if (!resultList.Contains(Model.Country)) resultList.Insert(0, Model.Country);
                else
                {
                    var country = new Country {Name = "Neues Land"};
                    resultList.Insert(0, country);
                }
                return resultList;
            }
        }

        public string Name
        {
            get { return Model.Name; }
            set
            {
                if(Model.Name == value)
                    return;
                Model.Name = value;
                OnPropertyChanged("Name");
            }
        }

        public string PostalCode
        {
            get { return Model.PostalCode; }
            set
            {
                if (Model.PostalCode == value)
                    return;
                Model.PostalCode = value;
                OnPropertyChanged("PostalCode");
            }
        }

        public Country Country
        {
            get { return Model.Country; }
            set
            {
                if (Model.Country == value)
                    return;
                Model.Country = value;
                OnPropertyChanged("Country");
            }
        }

        public bool IsValid
        {
            get { return Model.IsValid; }
        }
    }
}
