﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OrderListViewModel.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the OrderListViewModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.ObjectModel;
using System.Windows.Input;
using Framework.Various;
using Order = Framework.Models.Order;

namespace Framework.ViewModels
{
    public class OrderListViewModel : ViewModelBase
    {
        private ObservableCollection<Order> mModels;
        public ObservableCollection<Order> Models
        {
            get { return mModels; }

            set
            {
                if (mModels == value)
                    return;
                mModels = value;
                OnPropertyChanged("Models");
            }
        }

        private Order mSelectedModel;
        public Order SelectedModel
        {
            get { return mSelectedModel; }
            set
            {
                if (mSelectedModel == value)
                    return;
                mSelectedModel = value;
                OnPropertyChanged("SelectedModel");
            }
        }

        public ICommand AddCommand { get; set; }
        public ICommand EditCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
    }
}
