﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerAddViewModel.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the CustomerAddViewModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Framework.Models;
using Framework.Various;

namespace Framework.ViewModels
{
    internal class CustomerAddViewModel : ViewModelBase
    {
        public Customer Model { get; set; }
        public ICommand OkCommand { get; set; }
        public ICommand CancelCommand { get; set; }
        public ICommand AddCityCommand { get; set; }

        public IEnumerable<string> Titles
        {
            get { return from ETitle a in Enum.GetValues(typeof (ETitle)) select EnumDescriptionConverter.GetDescription(a); }
        }

        public Array CustomerTypes
        {
            get { return Enum.GetValues(typeof (ECustomerType)); }
        }

        public IList<City> Cities
        {
            get
            {
                IList<City> returnList =
                    new Repository<City>().Query()
                                          .OrderBy(x => x.Country)
                                          .Asc.OrderBy(x => x.PostalCode)
                                          .Asc.List<City>();
                if (!returnList.Contains(Model.Address.City)) returnList.Insert(0, Model.Address.City);
                else
                {
                    var city = new City {Name = "Neue Stadt anlegen"};
                    returnList.Insert(0, city);
                }
                return returnList;
            }
        }

        public string CustomerNumber
        {
            get { return Model.CustomerNumber; }
            set
            {
                if (Model.CustomerNumber == value)
                    return;
                Model.CustomerNumber = value;
                OnPropertyChanged("CustomerNumber");
            }
        }

        public string FirstName
        {
            get { return Model.FirstName; }
            set
            {
                if (Model.FirstName == value)
                    return;
                Model.FirstName = value;
                OnPropertyChanged("FirstName");
            }
        }

        public string Name
        {
            get { return Model.Name; }
            set
            {
                if (Model.Name == value)
                    return;
                Model.Name = value;
                OnPropertyChanged("Name");
            }
        }

        public ETitle Title
        {
            get { return Model.Title; }
            set
            {
                if (Model.Title == value)
                    return;
                Model.Title = value;
                OnPropertyChanged("Title");
            }
        }

        public ECustomerType Type
        {
            get { return Model.Type; }
            set
            {
                if (Model.Type == value)
                    return;
                if (value == ECustomerType.Organisation)
                {
                    Title = ETitle.NoTitle;
                    FirstName = "";
                }
                Model.Type = value;
                OnPropertyChanged("Type");
                OnPropertyChanged("HasFirstName");
            }
        }

        public string StreetNumber
        {
            get { return Model.Address.StreetNumber; }
            set
            {
                if (Model.Address.StreetNumber == value)
                    return;
                Model.Address.StreetNumber = value;
                OnPropertyChanged("StreetNumber");
            }
        }

        public City City
        {
            get { return Model.Address.City; }
            set
            {
                if (Model.Address.City == value)
                    return;
                Model.Address.City = value;
                OnPropertyChanged("City");
            }
        }

        public string Street
        {
            get { return Model.Address.Street; }
            set
            {
                if (Model.Address.Street == value)
                    return;
                Model.Address.Street = value;
                OnPropertyChanged("Street");
            }
        }

        public bool IsActive
        {
            get { return Model.IsActive; }
            set
            {
                if (Model.IsActive == value)
                    return;
                Model.IsActive = value;
                OnPropertyChanged("IsActive");
            }
        }

        public int HasFirstName
        {
            get { return Model.Type == ECustomerType.Person ? 35 : 0; }
        }

        public bool IsValid
        {
            get { return Model.IsValid; }
        }
    }
}