﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OrderImportListViewModel.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the OrderImportListViewModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.ObjectModel;
using System.Windows.Input;
using Framework.Various;
using Order = Framework.Models.Order;

namespace Framework.ViewModels
{
    public class OrderImportListViewModel : ViewModelBase
    {
        private ObservableCollection<Order> mModels;
        public ObservableCollection<Order> Models
        {
            get { return mModels; }

            set
            {
                if (mModels == value)
                    return;
                mModels = value;
                OnPropertyChanged("Models");
            }
        }

        private Order mSelectedModel;
        public Order SelectedModel
        {
            get { return mSelectedModel; }
            set
            {
                if (mSelectedModel == value)
                    return;
                mSelectedModel = value;
                OnPropertyChanged("SelectedModel");
            }
        }

        public ICommand UpdateCommand { get; set; }
        public ICommand ImportCommand { get; set; }
        public ICommand ImportAllCommand { get; set; }
        public ICommand DetailCommand { get; set; }
    }
}
