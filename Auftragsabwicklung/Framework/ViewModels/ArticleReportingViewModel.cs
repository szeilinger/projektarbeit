﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ArticleReportingViewModel.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the ArticleReportingViewModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.ObjectModel;
using System.Linq;
using Framework.Various;

namespace Framework.ViewModels
{
    class ArticleReportingViewModel : ReportingViewModel
    {
        public ObservableCollection<object> Models
        {
            get
            {
                bool hasDate = DateSpanTo != default(DateTime) && DateSpanFrom != default(DateTime);
                bool hasArea = ByArea && !string.IsNullOrWhiteSpace(Area);
                string query = @"
                        SELECT a.*, SUM(ol.Amount) AS Amount
                        FROM Articles a
                        INNER JOIN OrderLines ol ON a.Id=ol.ArticleId
                        INNER JOIN Orders o ON o.Id=ol.OrderId
                        INNER JOIN Employees e ON e.Id=o.EmployeeId" + "\n";
                if (hasDate)
                    query += string.Format(@"WHERE julianday(o.OrderDate) BETWEEN julianday('{0}') AND julianday('{1}')" + "\n",
                                           DateSpanFrom.ToString("yyyy-MM-dd"), DateSpanTo.ToString("yyyy-MM-dd"));
                if (hasArea)
                {
                    if (query.Contains("WHERE"))    query += "AND ";
                    else                            query += "WHERE ";
                    query += string.Format(@"e.Area='{0}'" + "\n", Area);
                }
                query += @"GROUP BY a.Id
                        ORDER BY Amount DESC
                        LIMIT 10";

                var session = HybridSessionBuilder.OpenSession();
                var zy = from a in session.CreateSQLQuery(query).List().Cast<object[]>() select new { Nummer = a[1], Name = a[2], Anzahl = a[7], Preis = a[4] };
                return new ObservableCollection<object>(zy.Cast<object>());
            }
        }
    }
}
