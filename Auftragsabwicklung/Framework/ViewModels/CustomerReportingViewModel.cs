﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerReportingViewModel.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the CustomerReportingViewModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.ObjectModel;
using System.Linq;
using Framework.Various;

namespace Framework.ViewModels
{
    class CustomerReportingViewModel : ReportingViewModel
    {
        public ObservableCollection<object> Models
        {
            get
            {
                bool hasDate = DateSpanTo != default(DateTime) && DateSpanFrom != default(DateTime);
                bool hasArea = ByArea && !string.IsNullOrWhiteSpace(Area);
                string query = @"
                        SELECT c.*, SUM(ol.Amount*a.Price) AS Revenue
                        FROM Customers c
                        INNER JOIN Orders o ON o.CustomerId=c.Id
                        INNER JOIN OrderLines ol ON ol.OrderId=o.Id
                        INNER JOIN Articles a ON ol.ArticleId=a.Id
                        INNER JOIN Employees e ON e.Id=o.EmployeeId" + "\n";
                if (hasDate)
                    query += string.Format(@"WHERE julianday(o.OrderDate) BETWEEN julianday('{0}') AND julianday('{1}')" + "\n",
                                           DateSpanFrom.ToString("yyyy-MM-dd"), DateSpanTo.ToString("yyyy-MM-dd"));
                if (hasArea)
                {
                    if (query.Contains("WHERE")) query += "AND ";
                    else query += "WHERE ";
                    query += string.Format(@"e.Area='{0}'" + "\n", Area);
                }
                query += @"GROUP BY c.Id
                        ORDER BY Revenue DESC
                        LIMIT 10";

                var session = HybridSessionBuilder.OpenSession();
                var zy = from a in session.CreateSQLQuery(query).List().Cast<object[]>() select new { Nummer = a[5], Name = string.Format("{0} {1}", a[3], a[2]), Aktiv = a[6], Umsatz = a[8] };
                return new ObservableCollection<object>(zy.Cast<object>());
            }
        }
    }
}
