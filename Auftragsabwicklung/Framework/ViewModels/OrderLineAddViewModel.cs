﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OrderLineAddViewModel.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the OrderLineAddViewModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Windows.Input;
using Framework.Models;
using Framework.Various;
using OrderLine = Framework.Models.OrderLine;

namespace Framework.ViewModels
{
    public class OrderLineAddViewModel : ViewModelBase
    {
        public OrderLine Model { get; set; }
        public ICommand OkCommand { get; set; }
        public ICommand CancelCommand { get; set; }
        public IList<Article> Articles
        {
            get
            {
                var returnList = new Repository<Article>().Query()
                            .Where(a => a.ValidFrom <= DateTime.Now && a.ValidTo >= DateTime.Now)
                            .List<Article>();
                return returnList;
            }
        }

        public Article Article
        {
            get { return Model.Article; }
            set
            {
                if (Model.Article == value)
                    return;
                Model.Article = value;
                OnPropertyChanged("Article");
            }
        }

        public int Amount
        {
            get { return Model.Amount; }
            set
            {
                if (Model.Amount == value)
                    return;
                Model.Amount = value;
                OnPropertyChanged("Amount");
            }
        }

        public int Position
        {
            get { return Model.Position; }
            set
            {
                if (Model.Position == value)
                    return;
                Model.Position = value;
                OnPropertyChanged("Position");
            }
        }

        public bool IsValid
        {
            get { return Model.IsValid; }
        }
    }
}
