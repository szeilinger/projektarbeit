﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OrderImportDetailViewModel.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the OrderImportDetailViewModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Framework.Various;
using Order = Framework.Models.Order;
using OrderLine = Framework.Models.OrderLine;

namespace Framework.ViewModels
{
    public class OrderImportDetailViewModel : ViewModelBase
    {
        #region Order
        public Order Model { get; set; }
        public ICommand OkCommand { get; set; }

        public string OrderNumber
        {
            get { return Model.OrderNumber; }
            set
            {
                if (Model.OrderNumber == value)
                    return;
                Model.OrderNumber = value;
                OnPropertyChanged("OrderNumber");
            }
        }
        public DateTime OrderDate
        {
            get { return Model.OrderDate; }
        }
        public string Description
        {
            get { return Model.Description; }
            set
            {
                if (Model.Description == value)
                    return;
                Model.Description = value;
                OnPropertyChanged("Description");
            }
        }
        public string Customer
        {
            get { return Model.Customer.ToString(); }
        }
        public string Employee
        {
            get { return Model.Employee.ToString(); }
        }
        public IList<OrderLine> OrderLines
        {
            get { return Model.OrderLines; }
            set
            {
                if (Model.OrderLines == value)
                    return;
                Model.OrderLines = value;
                OnPropertyChanged("OrderLines");
            }
        }
        #endregion

        #region OrderLines
        private ObservableCollection<OrderLine> mModels;
        public ObservableCollection<OrderLine> Models
        {
            get { return mModels; }

            set
            {
                if (mModels == value)
                    return;
                mModels = value;
                OnPropertyChanged("Models");
            }
        }

        private OrderLine mSelectedModel;
        public OrderLine SelectedModel
        {
            get { return mSelectedModel; }
            set
            {
                if (mSelectedModel == value)
                    return;
                mSelectedModel = value;
                base.OnPropertyChanged("SelectedModel");
            }
        }
        #endregion OrderLine
    }
}