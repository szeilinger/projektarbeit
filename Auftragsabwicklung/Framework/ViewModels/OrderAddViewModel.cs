﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OrderAddViewModel.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the OrderAddViewModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Framework.Models;
using Framework.Various;
using Order = Framework.Models.Order;
using OrderLine = Framework.Models.OrderLine;

namespace Framework.ViewModels
{
    public class OrderAddViewModel : ViewModelBase
    {
        #region Order
        public Order Model { get; set; }
        public ICommand OkCommand { get; set; }
        public ICommand CancelCommand { get; set; }

        public string OrderNumber
        {
            get { return Model.OrderNumber; }
            set
            {
                if (Model.OrderNumber == value)
                    return;
                Model.OrderNumber = value;
                OnPropertyChanged("OrderNumber");
            }
        }
        public DateTime OrderDate
        {
            get { return Model.OrderDate; }
            set
            {
                if (Model.OrderDate == value)
                    return;
                Model.OrderDate = value;
                OnPropertyChanged("OrderDate");
            }
        }
        public string Description
        {
            get { return Model.Description; }
            set
            {
                if (Model.Description == value)
                    return;
                Model.Description = value;
                OnPropertyChanged("Description");
            }
        }
        public Customer Customer
        {
            get { return Model.Customer; }
            set
            {
                if (Model.Customer == value)
                    return;
                Model.Customer = value;
                OnPropertyChanged("Customer");
            }
        }
        public Employee Employee
        {
            get { return Model.Employee; }
            set
            {
                if (Model.Employee == value)
                    return;
                Model.Employee = value;
                OnPropertyChanged("Employee");
            }
        }
        public IList<OrderLine> OrderLines
        {
            get { return Model.OrderLines; }
            set
            {
                if (Model.OrderLines.Equals(value))
                    return;
                Model.OrderLines = value;
                OnPropertyChanged("OrderLines");
            }
        }

        public bool IsValid
        {
            get { return Model.IsValid; }
        }

        public IList<Employee> Employees
        {
            get
            {
                return new Repository<Employee>().Query().Where(x => x.IsActive).List<Employee>();
            }
        }
        public IList<Customer> Customers
        {
            get
            {
                return new Repository<Customer>().Query().Where(x => x.IsActive).List<Customer>();
            }
        }
        #endregion

        #region OrderLines
        public ICommand AddCommand { get; set; }
        public ICommand EditCommand { get; set; }
        public ICommand DeleteCommand { get; set; }

        private ObservableCollection<OrderLine> mModels;
        public ObservableCollection<OrderLine> Models
        {
            get { return mModels; }

            set
            {
                if (mModels == value)
                    return;
                mModels = value;
                OnPropertyChanged("Models");
            }
        }

        private OrderLine mSelectedModel;
        public OrderLine SelectedModel
        {
            get { return mSelectedModel; }
            set
            {
                if (mSelectedModel == value)
                    return;
                mSelectedModel = value;
                base.OnPropertyChanged("SelectedModel");
            }
        }
        #endregion OrderLine
    }
}