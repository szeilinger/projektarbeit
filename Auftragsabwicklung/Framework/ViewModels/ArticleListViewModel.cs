﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ArticleListViewModel.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the ArticleListViewModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.ObjectModel;
using System.Windows.Input;
using Framework.Models;
using Framework.Various;

namespace Framework.ViewModels
{
    class ArticleListViewModel : ViewModelBase
    {
        private ObservableCollection<Article> mModels;
        public ObservableCollection<Article> Models
        {
            get { return mModels; }

            set
            {
                if (mModels == value)
                    return;
                mModels = value;
                OnPropertyChanged("Models");
            }
        }

        private Article mSelectedModel;
        public Article SelectedModel
        {
            get { return mSelectedModel; }
            set
            {
                if (mSelectedModel == value)
                    return;
                mSelectedModel = value;
                base.OnPropertyChanged("SelectedModel");
            }
        }

        public ICommand AddCommand { get; set; }
        public ICommand EditCommand { get; set; }
        public ICommand DeleteCommand { get; set; }
        public ICommand ImportCommand { get; set; }

    }
}
