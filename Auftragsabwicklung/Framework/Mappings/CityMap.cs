﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CityMap.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the CityMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using FluentNHibernate.Mapping;
using Framework.Models;

namespace Framework.Mappings
{
    class CityMap : ClassMap<City>
    {
        public CityMap()
        {
            Table("Cities");
            Id(x => x.Id);
            Map(x => x.PostalCode).Length(10).Not.Nullable();
            Map(x => x.Name).Length(100).Not.Nullable();
            References<Country>(x => x.Country).Column("CountryId").Fetch.Join().Cascade.SaveUpdate();
        }
    }
}
