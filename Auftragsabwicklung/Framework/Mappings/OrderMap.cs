﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OrderMap.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the OrderMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using FluentNHibernate.Mapping;
using Framework.Models;

namespace Framework.Mappings
{
    class OrderMap : ClassMap<Order>
    {
        public OrderMap()
        {
            Table("Orders");
            Id(x => x.Id);
            Map(x => x.OrderNumber).Length(50).Not.Nullable();
            Map(x => x.OrderDate).Not.Nullable();
            Map(x => x.Description).Length(500).Nullable();
            References<Customer>(x => x.Customer).Column("CustomerId").Fetch.Select().LazyLoad().Cascade.None();
            References<Employee>(x => x.Employee).Column("EmployeeId").Fetch.Select().LazyLoad().Cascade.None();
            HasMany<OrderLine>(x => x.OrderLines).KeyColumn("OrderId").Fetch.Select().LazyLoad().Not.Inverse().KeyNullable().Not.KeyUpdate().Cascade.AllDeleteOrphan();
        }
    }
}
