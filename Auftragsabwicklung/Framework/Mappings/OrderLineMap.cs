﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OrderLineMap.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the OrderLineMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using FluentNHibernate.Mapping;
using Framework.Models;

namespace Framework.Mappings
{
    class OrderLineMap : ClassMap<OrderLine>
    {
        public OrderLineMap()
        {
            Table("OrderLines");
            Id(x => x.Id);
            Map(x => x.Amount).Not.Nullable();
            Map(x => x.Position).Not.Nullable();
            References<Order>(x => x.Order).Not.Nullable().Column("OrderId").Fetch.Join().Cascade.None().UniqueKey("OrderArticle");
            References<Article>(x => x.Article).Not.Nullable().Column("ArticleId").Fetch.Join().Cascade.None().UniqueKey("OrderArticle");
        }
    }
}
