﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AdressMap.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the AdressMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using FluentNHibernate.Mapping;
using Framework.Models;

namespace Framework.Mappings
{
    class AdressMap : ClassMap<Adress>
    {
        public AdressMap()
        {
            Table("Adresses");
            Id(x => x.Id);
            Map(x => x.Street).Length(100).Nullable();
            Map(x => x.StreetNumber).Length(5).Nullable();
            References<City>(x => x.City).Column("CityId").Fetch.Join().Cascade.SaveUpdate();
        }
    }
}
