﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CountryMap.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the CountryMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using FluentNHibernate.Mapping;
using Framework.Models;

namespace Framework.Mappings
{
    class CountryMap : ClassMap<Country>
    {
        public CountryMap()
        {
            Table("Countries");
            Id(x => x.Id);
            Map(x => x.Name).Length(100).Not.Nullable();
        }
    }
}
