﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EmployeeMap.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the EmployeeMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using FluentNHibernate.Mapping;
using Framework.Models;

namespace Framework.Mappings
{
    internal class EmployeeMap : ClassMap<Employee>
    {
        public EmployeeMap()
        {
            Table("Employees");
            Id(x => x.Id);
            Map(x => x.EmployeeNumber).Not.Nullable().Unique();
            Map(x => x.LastName).Length(100).Not.Nullable();
            Map(x => x.FirstName).Length(100).Nullable();
            Map(x => x.Title).CustomType<ETitle>().Nullable();
            Map(x => x.Area).Length(10).Nullable();
            Map(x => x.IsActive).Not.Nullable();
            References<Adress>(x => x.Address).Column("AddressId").Fetch.Join().Cascade.All();
            HasMany<Order>(x => x.Orders).KeyColumn("EmployeeId").Fetch.Select().LazyLoad().Cascade.None();
        }
    }
}