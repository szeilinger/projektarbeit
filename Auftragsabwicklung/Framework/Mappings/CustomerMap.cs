﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerMap.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the CustomerMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using FluentNHibernate.Mapping;
using Framework.Models;

namespace Framework.Mappings
{
    class CustomerMap : ClassMap<Customer>
    {
        public CustomerMap()
        {
            Table("Customers");
            Id(x => x.Id);
            Map(x => x.Type).CustomType<ECustomerType>().Not.Nullable();
            Map(x => x.Name).Length(100).Not.Nullable();
            Map(x => x.FirstName).Length(100).Nullable();
            Map(x => x.Title).CustomType<ETitle>().Nullable();
            Map(x => x.CustomerNumber).Length(50).Not.Nullable();
            Map(x => x.IsActive).Not.Nullable();
            References<Adress>(x => x.Address).Column("AddressId").Fetch.Join().Cascade.All();
            HasMany<Order>(x => x.Orders).KeyColumn("CustomerId").Fetch.Select().LazyLoad().Cascade.None();
        }
    }
}
