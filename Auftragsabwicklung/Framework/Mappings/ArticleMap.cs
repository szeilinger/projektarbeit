﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ArticleMap.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the ArticleMap type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using FluentNHibernate.Mapping;
using Framework.Models;

namespace Framework.Mappings
{
    class ArticleMap : ClassMap<Article>
    {
        public ArticleMap()
        {
            Table("Articles");
            Id(x => x.Id);
            Map(x => x.ArticleNumber).Length(50).Not.Nullable();
            Map(x => x.Name).Length(100).Not.Nullable();
            Map(x => x.Description).Length(500).Nullable();
            Map(x => x.Price).Not.Nullable();
            Map(x => x.ValidFrom).Length(500).Not.Nullable();
            Map(x => x.ValidTo).Not.Nullable();
            HasMany<OrderLine>(x => x.OrderLines).KeyColumn("ArticleId").KeyNullable().Cascade.None();
        }
    }
}
