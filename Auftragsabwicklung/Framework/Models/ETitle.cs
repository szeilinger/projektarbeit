﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ETitle.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the ETitle type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.ComponentModel;

namespace Framework.Models
{
    public enum ETitle
    {
        [Description("Kein Titel")]
        NoTitle,
        [Description("Dipl. Ing.")]
        DiplIng,
        [Description("Dr.")]
        Dr,
        [Description("Dr. Dr.")]
        DrDr,
        [Description("Prof.")]
        Prof,
        [Description("Prof. Dr.")]
        ProfDr,
        [Description("Prof. Dr. Dr.")]
        ProfDrDr
    }
}
