﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ECustomerType.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the ECustomerType type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Framework.Models
{
    public enum ECustomerType
    {
        Organisation,
        Person
    }
}
