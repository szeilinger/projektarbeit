// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EMonth.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the EMonth type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.ComponentModel;

namespace Framework.Models
{
    internal enum EMonth
    {
        [Description("Kein Monat gew�hlt")]
        NONE = 0,
        [Description("Januar")]
        JANUAR = 1,
        [Description("Februar")]
        FEBRUAR = 2,
        [Description("M�rz")]
        MAERZ = 3,
        [Description("April")]
        APRIL = 4,
        [Description("Mai")]
        MAI = 5,
        [Description("Juni")]
        JUNI = 6,
        [Description("Juli")]
        JULI = 7,
        [Description("August")]
        AUGUST = 8,
        [Description("September")]
        SEPTEMBER = 9,
        [Description("Oktober")]
        OKTOBER = 10,
        [Description("November")]
        NOVEMBER = 11,
        [Description("Dezember")]
        DEZEMBER = 12
    }
}
