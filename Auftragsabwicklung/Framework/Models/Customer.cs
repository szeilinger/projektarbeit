﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Customer.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the Customer type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;

namespace Framework.Models
{
    public class Customer
    {
        public int Id { get; set; }
        public ECustomerType Type { get; set; }
        public string Name { get; set; }
        public string FirstName { get; set; }
        public ETitle Title { get; set; }
        public string CustomerNumber { get; set; }
        public bool IsActive { get; set; }
        public Adress Address { get; set; }
        public IList<Order> Orders { get; set; }

        public Customer()
        {
            Address = new Adress();
            Orders = new List<Order>();
        }

        public override string ToString()
        {
            if (!string.IsNullOrWhiteSpace(FirstName))
                return CustomerNumber + ": " + FirstName + " " + Name;
            else
                return CustomerNumber + ": " + Name;
        }

        public bool IsValid
        {
            get { return !string.IsNullOrWhiteSpace(CustomerNumber) && !string.IsNullOrWhiteSpace(Name) && Address != null && Address.IsValid; }
        }
    }
}
