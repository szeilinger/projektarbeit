﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Article.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the Article type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using Framework.Views.Controls;

namespace Framework.Models
{
    public class Article
    {
        public int Id { get; set; }
        public string ArticleNumber { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public DateTime ValidFrom { get; set; }
        public DateTime ValidTo { get; set; }
        public IList<OrderLine> OrderLines { get; set; }

        public Article()
        {
            ValidFrom = DateTime.Today;
            ValidTo = DateTime.Today.AddYears(1);
        }

        public override string ToString()
        {
            return ArticleNumber + ": " + Name;
        }

        public bool IsValid
        {
            get
            {
                return !string.IsNullOrWhiteSpace(Name) && !string.IsNullOrWhiteSpace(ArticleNumber) &&
                       ValidFrom != default(DateTime) && ValidTo != default(DateTime);
            }
        }
    }
}
