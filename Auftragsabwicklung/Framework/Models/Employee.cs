﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Employee.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the Employee type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.Generic;

namespace Framework.Models
{
    public class Employee
    {
        public int Id { get; set; }
        public int EmployeeNumber { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public ETitle Title { get; set; }
        public string Area { get; set; }
        public bool IsActive { get; set; }
        public Adress Address { get; set; }
        public IList<Order> Orders { get; set; }

        public Employee()
        {
            Address = new Adress();
            Orders = new List<Order>();
        }

        public override string ToString()
        {
            return EmployeeNumber + ": " + FirstName + " " + LastName;
        }

        public bool IsValid
        {
            get { return EmployeeNumber != default(int) && !string.IsNullOrWhiteSpace(FirstName) && !string.IsNullOrWhiteSpace(LastName) && Address != null && Address.IsValid; }
        }
    }
}
