﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OrderLine.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the OrderLine type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Framework.Models
{
    public class OrderLine
    {
        public int Id { get; set; }
        public int Amount { get; set; }
        public int Position { get; set; }
        public Order Order { get; set; }
        public Article Article { get; set; }

        public OrderLine()
        {
            Order = new Order();
            Article = new Article();
        }

        public override string ToString()
        {
            return Position + ": " + Amount + " x " + Article.ArticleNumber;
        }

        public bool IsValid
        {
            get { return Position != default(int) && Amount != default(int) && Article != null && Order != null; }
        }
    }
}
