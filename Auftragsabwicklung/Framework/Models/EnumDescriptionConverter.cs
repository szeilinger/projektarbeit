﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EnumDescriptionConverter.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the EnumDescriptionConverter type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Windows.Data;

namespace Framework.Models
{
    public class EnumDescriptionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is Enum)) throw new ArgumentException("Value is not an Enum");
            return GetDescription(value as Enum);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is string)) throw new ArgumentException("Value is not a string");

            var items = from object item in Enum.GetValues(targetType)
                        let asString = GetDescription(item as Enum)
                        where asString == (string) value
                        select item;
            var result = items.FirstOrDefault();
            if (result != null)
                return result;
            throw new ArgumentException("Unable to match string to Enum description");
        }

        internal static string GetDescription(Enum enumObj)
        {
            var fieldInfo = enumObj.GetType().GetField(enumObj.ToString());
            var attribArray = fieldInfo.GetCustomAttributes(false);

            if (attribArray.Length == 0)
            {
                return enumObj.ToString();
            }
            var attrib = (DescriptionAttribute)attribArray[0];
            return attrib.Description;
        }
    }
}
