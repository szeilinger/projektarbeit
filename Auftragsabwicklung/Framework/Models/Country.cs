﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Country.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the Country type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Framework.Models
{
    public class Country
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Country()
        {
            Name = "";
        }

        public override string ToString()
        {
            if (string.IsNullOrWhiteSpace(Name)) return "Neues Land";
            return Name;
        }

        public bool IsValid
        {
            get { return !string.IsNullOrWhiteSpace(Name); }
        }
    }
}
