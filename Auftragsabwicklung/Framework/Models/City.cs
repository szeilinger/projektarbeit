﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="City.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the City type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Framework.Models
{
    public class City
    {
        public int Id { get; set; }
        public string PostalCode { get; set; }
        public string Name { get; set; }
        public Country Country { get; set; }

        public City()
        {
            Country = new Country();
        }

        public override string ToString()
        {
            if (string.IsNullOrWhiteSpace(PostalCode + Name))
            {
                return "Neue Stadt";
            }
            string result = "";
            if (!string.IsNullOrWhiteSpace(PostalCode))
                result += PostalCode + " ";
            result += Name;
            if (!string.IsNullOrWhiteSpace(Country.ToString()))
                result += ", " + Country.ToString();
            return result;
        }

        public bool IsValid
        {
            get { return !string.IsNullOrWhiteSpace(Name) && !string.IsNullOrWhiteSpace(PostalCode) && Country != null && Country.IsValid; }
        }
    }
}
