﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Order.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the Order type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace Framework.Models
{
    public class Order
    {
        public int Id { get; set; }
        public string OrderNumber { get; set; }
        public DateTime OrderDate { get; set; }
        public string Description { get; set; }
        public Customer Customer { get; set; }
        public Employee Employee { get; set; }
        public IList<OrderLine> OrderLines { get; set; }

        public Order()
        {
            Customer = new Customer();
            Employee = new Employee();
            OrderLines = new List<OrderLine>();
            OrderDate = DateTime.Today.AddYears(1);
        }

        public override string ToString()
        {
            return OrderNumber + ": " + OrderDate.ToString("dd.MM.yyyy");
        }

        public bool IsValid
        {
            get
            {
                return !string.IsNullOrWhiteSpace(OrderNumber) && OrderDate != default(DateTime) && Customer != null &&
                       Employee != null;
            }
        }
    }
}
