﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Adress.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the Adress type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Framework.Models
{
    public class Adress
    {
        public int Id { get; set; }
        public string Street { get; set; }
        public string StreetNumber { get; set; }
        public City City { get; set; }

        public Adress()
        {
            City = new City();
        }

        public override string ToString()
        {
            return Street + " " + StreetNumber + ", " + City.ToString();
        }

        public bool IsValid
        {
            get
            {
                return !string.IsNullOrWhiteSpace(Street) && !string.IsNullOrWhiteSpace(StreetNumber) && City != null &&
                       City.IsValid;
            }
        }
    }
}
