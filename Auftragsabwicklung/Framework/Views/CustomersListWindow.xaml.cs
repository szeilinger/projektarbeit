﻿using Framework.Views.Controls;

namespace Framework.Views
{
    /// <summary>
    /// Interaction logic for CustomersListWindow.xaml
    /// </summary>
    public partial class CustomersListWindow : LabeledControl
    {
        public CustomersListWindow()
        {
            InitializeComponent();
        }
    }
}
