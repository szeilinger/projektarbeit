﻿using System.Windows;

namespace Framework.Views
{
    /// <summary>
    /// Interaction logic for ArticleAddWindow.xaml
    /// </summary>
    public partial class ArticleAddWindow : Window
    {
        public ArticleAddWindow()
        {
            InitializeComponent();
        }
    }
}
