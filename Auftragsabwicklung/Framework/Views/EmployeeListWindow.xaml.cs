﻿using Framework.Views.Controls;

namespace Framework.Views
{
    /// <summary>
    /// Interaction logic for EmployeesListWindow.xaml
    /// </summary>
    public partial class EmployeeListWindow : LabeledControl
    {
        public EmployeeListWindow()
        {
            InitializeComponent();
        }
    }
}
