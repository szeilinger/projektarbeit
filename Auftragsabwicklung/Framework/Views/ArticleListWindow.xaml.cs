﻿using Framework.Views.Controls;

namespace Framework.Views
{
    /// <summary>
    /// Interaction logic for ArticleListWindow.xaml
    /// </summary>
    public partial class ArticleListWindow : LabeledControl
    {
        public ArticleListWindow()
        {
            InitializeComponent();
        }
    }
}
