﻿using System.Windows;

namespace Framework.Views
{
    /// <summary>
    /// Interaction logic for EmployeeAddWindow.xaml
    /// </summary>
    public partial class EmployeeAddWindow : Window
    {
        public EmployeeAddWindow()
        {
            InitializeComponent();
        }
    }
}
