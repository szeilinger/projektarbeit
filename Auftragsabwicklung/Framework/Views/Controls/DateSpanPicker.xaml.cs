﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;

namespace Framework.Views.Controls
{
    /// <summary>
    /// Interaction logic for DateSpanPicker.xaml
    /// </summary>
    public partial class DateSpanPicker : UserControl
    {
        private IEnumerable<int> Years
        {
            get
            {
                var currentYear = DateTime.Today.Year;
                for (var i = -10; i <= 10; i++)
                {
                    yield return currentYear + i;
                }
            }
        }
        private IEnumerable<int> Months
        {
            get { return new [] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}; }
        }


        public DateSpanPicker()
        {
            InitializeComponent();
        }
    }
}
