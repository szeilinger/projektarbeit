﻿using System.ComponentModel;
using System.Windows.Controls;


namespace Framework.Views.Controls
{
    /// <summary>
    /// Interaction logic for LabeledControl.xaml
    /// </summary>
    public partial class LabeledControl : UserControl
    {
        /// <summary>
        /// The Controls caption
        /// </summary>
        [Description("Gets or sets the Controls Caption"), Category("Layout"), DefaultValue(""), Browsable(true)]
        public string Caption { get; set; }
    }
}
