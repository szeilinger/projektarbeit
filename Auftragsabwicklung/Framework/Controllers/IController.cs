﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IController.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the IController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Windows.Controls;

namespace Framework.Controllers
{
    public interface IController
    {
        Control Initialize();
    }
}
