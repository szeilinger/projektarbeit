﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CityAddController.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the CityAddController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Framework.Models;
using Framework.Various;
using Framework.ViewModels;
using Framework.Views;

namespace Framework.Controllers
{
    class CityAddController
    {
        private CityAddWindow mView;
        private CityAddViewModel mViewModel;

        public City AddCity()
        {
            mView = new CityAddWindow();
            mViewModel = new CityAddViewModel()
            {
                Model = new City(),
                OkCommand = new RelayCommand(ExecuteOkCommand, (o) => mViewModel.IsValid ),
                CancelCommand = new RelayCommand(ExecuteCancelCommand),
                AddCountryCommand = new RelayCommand(ExecuteAddCountryCommand)
            };

            mView.DataContext = mViewModel;
            mView.Title = "Stadt anlegen";

            return mView.ShowDialog() == true ? mViewModel.Model : null;
        }

        private void ExecuteAddCountryCommand(object obj)
        {
            var c = new CountryAddController();
            c.EditCountry(mViewModel.Model.Country);
        }

        private void ExecuteOkCommand(object obj)
        {
            mView.DialogResult = true;
            mView.Close();
        }

        private void ExecuteCancelCommand(object obj)
        {
            mView.DialogResult = false;
            mView.Close();
        }

        internal City EditCity(City arti)
        {
            mView = new CityAddWindow();
            mViewModel = new CityAddViewModel()
            {
                Model = arti,
                OkCommand = new RelayCommand(ExecuteOkCommand, (o) => mViewModel.IsValid),
                CancelCommand = new RelayCommand(ExecuteCancelCommand),
                AddCountryCommand = new RelayCommand(ExecuteAddCountryCommand)
            };

            mView.DataContext = mViewModel;
            mView.Title = "Stadt bearbeiten";

            return mView.ShowDialog() == true ? mViewModel.Model : null;
        }
    }
}
