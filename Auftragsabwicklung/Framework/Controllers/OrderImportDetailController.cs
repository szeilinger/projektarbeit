﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OrderImportDetailController.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the OrderImportDetailController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.ObjectModel;
using Framework.Various;
using Framework.ViewModels;
using Framework.Views;
using Order = Framework.Models.Order;
using OrderLine = Framework.Models.OrderLine;

namespace Framework.Controllers
{
    class OrderImportDetailController
    {
        private OrderImportDetailWindow mView;
        private OrderImportDetailViewModel mViewModel;

        private void CreateViewModel(Order order)
        {
            mViewModel = new OrderImportDetailViewModel()
            {
                OkCommand = new RelayCommand(ExecuteOkCommand)
            };

            if (order == null)
            {
                mViewModel.Model = new Order();
                mViewModel.Models = new ObservableCollection<OrderLine>();
            }
            else
            {
                mViewModel.Model = order;
                mViewModel.Models = new ObservableCollection<OrderLine>(order.OrderLines);
            }
        }

        private void ExecuteOkCommand(object obj)
        {
            mView.DialogResult = true;
            mView.Close();
        }

        internal Order ShowDetailOrder(Order order)
        {
            mView = new OrderImportDetailWindow();
            CreateViewModel(order);

            mView.DataContext = mViewModel;
            mView.ShowDialog();
            return mViewModel.Model;
        }
    }
}