﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OrderLineAddController.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the OrderLineAddController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Linq;
using Framework.Various;
using Framework.ViewModels;
using Framework.Views;
using Order = Framework.Models.Order;
using OrderLine = Framework.Models.OrderLine;

namespace Framework.Controllers
{
    class OrderLineAddController
    {
        private OrderLineAddWindow mView;
        private OrderLineAddViewModel mViewModel;

        public OrderLine AddOrderLine(Order parent)
        {
            var nextNumber =
                (from ol in parent.OrderLines orderby ol.Position descending select ol.Position).FirstOrDefault();

            mView = new OrderLineAddWindow();
            var orderLine = new OrderLine { Order = parent };

            orderLine.Position = nextNumber + 100;
            CreateViewModel(orderLine);

            mView.DataContext = mViewModel;
            mView.Title = "Position anlegen";

            return mView.ShowDialog() == true ? mViewModel.Model : null;
        }

        private void CreateViewModel(OrderLine orderLine)
        {
            mViewModel = new OrderLineAddViewModel()
            {
                OkCommand = new RelayCommand(ExecuteOkCommand, (o) => mViewModel.IsValid),
                CancelCommand = new RelayCommand(ExecuteCancelCommand)
            };

            mViewModel.Model = orderLine ?? new OrderLine();
        }

        private void ExecuteOkCommand(object obj)
        {
            mView.DialogResult = true;
            mView.Close();
        }

        private void ExecuteCancelCommand(object obj)
        {
            mView.DialogResult = false;
            mView.Close();
        }

        internal OrderLine EditOrderLine(OrderLine orderLine)
        {
            mView = new OrderLineAddWindow();
            CreateViewModel(orderLine);

            mView.DataContext = mViewModel;
            mView.Title = "Position bearbeiten";

            return mView.ShowDialog() == true ? mViewModel.Model : null;
        }
    }
}
