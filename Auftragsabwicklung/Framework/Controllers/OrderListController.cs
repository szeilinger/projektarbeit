﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OrderListController.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the OrderListController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Framework.Various;
using Framework.ViewModels;
using Framework.Views;
using Order = Framework.Models.Order;

namespace Framework.Controllers
{
    class OrderListController : IController
    {
        private Repository<Order> mOrderRepository;
        private OrderListViewModel mViewModel;

        public Control Initialize()
        {
            mOrderRepository = new Repository<Order>();

            var view = new OrderListWindow();
            mViewModel = new OrderListViewModel
            {
                Models = new ObservableCollection<Order>(mOrderRepository.GetAll()),
                AddCommand = new RelayCommand(AddCommandExecute),
                EditCommand = new RelayCommand(EditCommandExecute, DeleteCommandCanExecute),
                DeleteCommand = new RelayCommand(DeleteCommandExecute, DeleteCommandCanExecute)
            };

            view.DataContext = mViewModel;
            return view;
        }

        private void AddCommandExecute(object obj)
        {
            var addedObject = new OrderAddController().AddOrder();
            if (addedObject != null)
            {
                mOrderRepository.Save(addedObject);
                mViewModel.Models.Add(addedObject);
            }
        }

        private void EditCommandExecute(object obj)
        {
            var editedObject = new OrderAddController().EditOrder(mViewModel.SelectedModel);
            if (editedObject != null)
            {
                mOrderRepository.Update(editedObject);
                int selected = mViewModel.Models.IndexOf(editedObject);
                mViewModel.Models = new ObservableCollection<Order>(mOrderRepository.GetAll());
                mViewModel.SelectedModel = mViewModel.Models.ElementAt(selected);
            }
            HybridSessionBuilder.OpenSession().Refresh(mViewModel.SelectedModel);
        }

        private void DeleteCommandExecute(object obj)
        {
            var result = MessageBox.Show("Möchten Sie diesen Auftrag " + mViewModel.SelectedModel.OrderNumber + " wirklich löschen?", "Warnung", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.No)
            {
                return;
            }
            mOrderRepository.Delete(mViewModel.SelectedModel);
            mViewModel.Models.Remove(mViewModel.SelectedModel);
        }

        private bool DeleteCommandCanExecute(object obj)
        {
            return mViewModel.SelectedModel != null;
        }
    }
}
