﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ArticleXmlImportController.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the ArticleXmlImportController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Xml.Linq;
using Framework.Models;
using Framework.Various;
using Microsoft.Win32;

namespace Framework.Controllers
{
    internal class ArticleXmlImportController
    {
        public static IList<Article> LoadArcticleXml()
        {
            var ofd = new OpenFileDialog();
            ofd.ShowDialog();
            string file = ofd.FileName;
            XDocument anotherDoc = XDocument.Load(file);
            var importedArticles = from c in anotherDoc.Descendants("Artikel")
                                   select new Article()
                                              {
                                                  ArticleNumber = c.Element("Artikelnummer").Value,
                                                  Name = c.Element("Name").Value,
                                                  Price =
                                                      double.Parse(c.Element("Preis").Value,
                                                                   System.Globalization.NumberStyles
                                                                         .AllowDecimalPoint,
                                                                   System.Globalization.NumberFormatInfo
                                                                         .InvariantInfo),
                                                  ValidFrom = Convert.ToDateTime(c.Element("GueltigAb").Value),
                                                  ValidTo = Convert.ToDateTime(c.Element("GueltigBis").Value)
                                              };
            var repo = new Repository<Article>();
            foreach (var art in importedArticles)
            {
                //repo.Save(art);
            }
            return importedArticles.ToList();
        }
    }
}
