﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ArticleAddController.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger  
// </copyright>
// <summary>
//   Defines the ArticleAddController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Framework.Models;
using Framework.Various;
using Framework.ViewModels;
using Framework.Views;

namespace Framework.Controllers
{
    class ArticleAddController
    {
        private ArticleAddWindow mView;
        private ArticleAddViewModel mViewModel;

        public Article AddArticle()
        {
            mView = new ArticleAddWindow();
            mViewModel = new ArticleAddViewModel()
            {
                Model = new Article(),
                OkCommand = new RelayCommand(ExecuteOkCommand, (o) => mViewModel.IsValid),
                CancelCommand = new RelayCommand(ExecuteCancelCommand),
            };

            mView.DataContext = mViewModel;
            mView.Title = "Artikel anlegen";

            return mView.ShowDialog() == true ? mViewModel.Model : null;
        }

        private void ExecuteOkCommand(object obj)
        {
            mView.DialogResult = true;
            mView.Close();
        }

        private void ExecuteCancelCommand(object obj)
        {
            mView.DialogResult = false;
            mView.Close();
        }

        internal Article EditArticle(Article article)
        {
            mView = new ArticleAddWindow();
            mViewModel = new ArticleAddViewModel()
            {
                Model = article,
                OkCommand = new RelayCommand(ExecuteOkCommand, (o) => mViewModel.IsValid),
                CancelCommand = new RelayCommand(ExecuteCancelCommand)
            };

            mView.DataContext = mViewModel;
            mView.Title = "Artikel bearbeiten";

            return mView.ShowDialog() == true ? mViewModel.Model : null;
        }
    }
}