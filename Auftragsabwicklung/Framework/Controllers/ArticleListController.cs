﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ArticleListController.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the ArticleListController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Framework.Models;
using Framework.Various;
using Framework.ViewModels;
using Framework.Views;

namespace Framework.Controllers
{
    internal class ArticleListController : IController
    {
        private Repository<Article> mArticleRepository;
        private ArticleListViewModel mViewModel;

        public Control Initialize()
        {
            mArticleRepository = new Repository<Article>();

            var view = new ArticleListWindow();
            mViewModel = new ArticleListViewModel
                             {
                                 Models = new ObservableCollection<Article>(mArticleRepository.GetAll()),
                                 AddCommand = new RelayCommand(AddCommandExecute),
                                 ImportCommand = new RelayCommand(ImportCommand),
                                 EditCommand = new RelayCommand(EditCommandExecute, DeleteCommandCanExecute),
                                 DeleteCommand = new RelayCommand(DeleteCommandExecute, DeleteCommandCanExecute)
                             };

            view.DataContext = mViewModel;
            return view;
        }

        private void AddCommandExecute(object obj)
        {
            var addedObject = new ArticleAddController().AddArticle();
            if (addedObject != null)
            {
                mArticleRepository.Save(addedObject);
                mViewModel.Models.Add(addedObject);
            }
        }

        private void ImportCommand(object obj)
        {
            var x = ArticleXmlImportController.LoadArcticleXml();
            foreach (var article in x)
            {
                if(mArticleRepository.Query().Where(a=>a.ArticleNumber==article.ArticleNumber).RowCount()>0)
                {
                    MessageBox.Show("Der Artikel mit der Artikelnummer " + article.ArticleNumber + " existiert bereits!");
                    continue;
                }
                mViewModel.Models.Add(article);
                mArticleRepository.Save(article);
            }
        }

        private void EditCommandExecute(object obj)
        {
            var editedObject = new ArticleAddController().EditArticle(mViewModel.SelectedModel);
            if (editedObject != null)
            {
                mArticleRepository.Update(editedObject);
                int selected = mViewModel.Models.IndexOf(editedObject);
                mViewModel.Models = new ObservableCollection<Article>(mArticleRepository.GetAll());
                mViewModel.SelectedModel = mViewModel.Models.ElementAt(selected);
            }
            HybridSessionBuilder.OpenSession().Refresh(mViewModel.SelectedModel);
        }

        private void DeleteCommandExecute(object obj)
        {
            if (mViewModel.SelectedModel != null)
            {
                var result = MessageBox.Show("Möchten Sie wirklich diesen Artikel löschen?", "Warnung", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.No)
                {
                    return;
                }
                try
                {
                    mArticleRepository.Delete(mViewModel.SelectedModel);
                    mViewModel.Models.Remove(mViewModel.SelectedModel);
                }
                catch (Exception)
                {
                    MessageBox.Show("Dieser Artikel kann nicht gelöscht werden!\nBitte löschen Sie erst den Auftrag!");
                }
            }
        }

        private bool DeleteCommandCanExecute(object obj)
        {
            return mViewModel.SelectedModel != null;
        }
    }
}
