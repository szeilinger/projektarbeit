﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerReportingController.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the CustomerReportingController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Windows.Controls;
using Framework.ViewModels;
using Framework.Views;

namespace Framework.Controllers
{
    internal class CustomerReportingController : IController
    {
        public Control Initialize()
        {
            var mViewModel = new CustomerReportingViewModel();
            var mView = new CustomerReportingWindow();
            mView.DataContext = mViewModel;
            return mView;
        }
    }
}
