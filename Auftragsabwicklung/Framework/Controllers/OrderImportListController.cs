﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OrderImportListController.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the OrderImportListController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Framework.Models;
using Framework.Various;
using Framework.ViewModels;
using Framework.Views;
using Order = Framework.Models.Order;
using OrderLine = Framework.Models.OrderLine;

namespace Framework.Controllers
{
    class OrderImportListController : IController, IDisposable
    {
        private Repository<Order> mOrderRepository;
        private OrderImportListViewModel mViewModel;
        private VaccuWebOrderServiceClient client;
        private Task<bool> login;
        private string[] openOrderNumbers;
        private IList<Order> openOrders;

        public Control Initialize()
        {
            client = new VaccuWebOrderServiceClient();
            login = client.LoginAsync(new Guid("{D14A8617-36A6-4D33-B430-E9510287DF1F}"));

            mOrderRepository = new Repository<Order>();

            var view = new OrderImportListWindow();
            mViewModel = new OrderImportListViewModel
                             {
                                 UpdateCommand = new RelayCommand(UpdateCommandExecute),
                                 ImportCommand = new RelayCommand(ImportCommandExecute, ImportCommandCanExecute),
                                 ImportAllCommand = new RelayCommand(ImportAllCommandExecute, ImportAllCommandCanExecute),
                                 DetailCommand = new RelayCommand(DetailCommandExecute, ImportCommandCanExecute)
                             };

            view.DataContext = mViewModel;
            UpdateCommandExecute(null);
            return view;
        }

        private void DetailCommandExecute(object o)
        {
            var editedObject = new OrderImportDetailController().ShowDetailOrder(mViewModel.SelectedModel);
            int selected = mViewModel.Models.IndexOf(editedObject);
            mViewModel.Models.RemoveAt(selected);
            mViewModel.Models.Insert(selected, editedObject);
        }

        private void UpdateCommandExecute(object obj)
        {
            if(login.Status == TaskStatus.Running) login.Wait();
            if (login.IsFaulted || !login.Result)
            {
                MessageBox.Show("Verbindung zum Service oder Login fehlgeschlagen!", "Fehler", MessageBoxButton.OK,
                                MessageBoxImage.Error, MessageBoxResult.OK);
                return;
            }

            openOrderNumbers = client.GetOpenOrders();
            openOrders = new List<Order>();
            var customerRepo = new Repository<Customer>();
            var employeeRepo = new Repository<Employee>();
            var articleRepo = new Repository<Article>();
            foreach (var x in openOrderNumbers)
            {
                var serviceOrder = client.GetOrder(x);
                var order = OrderFromServiceOrder(serviceOrder, customerRepo, employeeRepo);
                openOrders.Add(order);

                foreach (var serviceOrderLine in serviceOrder.OrderLines)
                {
                    var orderLine = order.OrderLines.FirstOrDefault(xl => xl.Article.ArticleNumber == serviceOrderLine.ArticleNumber);
                    if (orderLine != null)
                    {
                        orderLine.Amount += serviceOrderLine.Amount;
                        continue;
                    }
                    orderLine = OrderLineFromServiceOrderLine(order, serviceOrderLine, articleRepo);
                    order.OrderLines.Add(orderLine);
                }
            }

            mViewModel.Models = new ObservableCollection<Order>(openOrders);
        }

        private static OrderLine OrderLineFromServiceOrderLine(Order o, Various.OrderLine l, Repository<Article> articleRepo)
        {
            return new OrderLine
                       {
                           Order = o,
                           Position = l.Sequence,
                           Amount = l.Amount,
                           Article = articleRepo.Query()
                                                .Where(a => a.ArticleNumber == l.ArticleNumber)
                                                .List<Article>().FirstOrDefault()
                       };
        }

        private static Order OrderFromServiceOrder(Various.Order order, Repository<Customer> customerRepo, Repository<Employee> employeeRepo)
        {
            return new Order
                       {
                           OrderNumber = order.OrderNumber,
                           OrderDate = order.OrderDate,
                           Customer = customerRepo.Query()
                                                  .Where(c => c.CustomerNumber == order.CustomerNumber)
                                                  .List<Customer>().FirstOrDefault(),
                           Employee = employeeRepo.Query()
                                                  .Where( e => e.EmployeeNumber == Convert.ToInt32(order.EmployeeNumber))
                                                  .List<Employee>().FirstOrDefault()
                       };
        }

        private void ImportCommandExecute(object obj)
        {
            var currentOrder = mViewModel.SelectedModel;
            mOrderRepository.Save(currentOrder);
            mViewModel.Models.Remove(currentOrder);
            client.CloseOrder(currentOrder.OrderNumber);
        }

        private void ImportAllCommandExecute(object obj)
        {
            foreach (var openOrder in mViewModel.Models)
            {
                mOrderRepository.Save(openOrder);
                client.CloseOrder(openOrder.OrderNumber);
            }
            mViewModel.Models.Clear();
            UpdateCommandExecute(null);
        }
        private bool ImportCommandCanExecute(object obj)
        {
            return mViewModel.SelectedModel != null;
        }
        private bool ImportAllCommandCanExecute(object obj)
        {
            return mViewModel.Models.Count > 0;
        }

        public void Dispose()
        {
            client.Close();
        }
    }
}
