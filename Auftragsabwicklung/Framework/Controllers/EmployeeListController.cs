﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EmployeeListController.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the EmployeeListController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Framework.Models;
using Framework.Various;
using Framework.ViewModels;
using Framework.Views;

namespace Framework.Controllers
{
    class EmployeeListController : IController
    {
        private Repository<Employee> mEmployeeRepository;
        private EmployeeListViewModel mViewModel;

        public Control Initialize()
        {
            mEmployeeRepository = new Repository<Employee>();

            var view = new EmployeeListWindow();
            mViewModel = new EmployeeListViewModel
            {
                Models = new ObservableCollection<Employee>(mEmployeeRepository.GetAll()),
                AddCommand = new RelayCommand(AddCommandExecute),
                EditCommand = new RelayCommand(EditCommandExecute, DeleteCommandCanExecute),
                DeleteCommand = new RelayCommand(DeleteCommandExecute, DeleteCommandCanExecute)
            };

            view.DataContext = mViewModel;
            return view;
        }

        private void AddCommandExecute(object obj)
        {
            var addedObject = new EmployeeAddController().AddEmployee();
            if (addedObject != null)
            {
                mEmployeeRepository.Save(addedObject);
                mViewModel.Models.Add(addedObject);
            }
        }

        private void EditCommandExecute(object obj)
        {
            var editedObject = new EmployeeAddController().EditEmployee(mViewModel.SelectedModel);
            if (editedObject != null)
            {
                mEmployeeRepository.Update(editedObject);
                int selected = mViewModel.Models.IndexOf(editedObject);
                mViewModel.Models = new ObservableCollection<Employee>(mEmployeeRepository.GetAll());
                mViewModel.SelectedModel = mViewModel.Models.ElementAt(selected);
            }
            HybridSessionBuilder.OpenSession().Refresh(mViewModel.SelectedModel);
        }

        private void DeleteCommandExecute(object obj)
        {
            if (mViewModel.SelectedModel != null)
            {
                var result = MessageBox.Show("Möchten Sie diesen Mitarbeiter " + mViewModel.SelectedModel.EmployeeNumber + " wirklich löschen?", "Warnung", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.No)
                {
                    return;
                }
                try
                {
                    mEmployeeRepository.Delete(mViewModel.SelectedModel);
                    mViewModel.Models.Remove(mViewModel.SelectedModel);
                }
                catch (Exception)
                {
                    MessageBox.Show("Dieser Mitarbeiter kann nicht gelöscht werden!\nBitte löschen Sie erst den Auftrag!");
                }
            }
        }
        private bool DeleteCommandCanExecute(object obj)
        {
            return mViewModel.SelectedModel != null;
        }
    }
}