﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ArticleReportingController.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the ArticleReportingController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Windows.Controls;
using Framework.ViewModels;
using Framework.Views;

namespace Framework.Controllers
{
    internal class ArticleReportingController : IController
    {
        public Control Initialize()
        {
            var mViewModel = new ArticleReportingViewModel();
            var mView = new ArticleReportingWindow();
            mView.DataContext = mViewModel;
            return mView;
        }
    }
}
