﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EmployeeReportingController.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the EmployeeReportingController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Windows.Controls;
using Framework.ViewModels;
using Framework.Views;

namespace Framework.Controllers
{
    class EmployeeReportingController : IController
    {
        public Control Initialize()
        {
            var mViewModel = new EmployeeReportingViewModel();
            var mView = new EmployeeReportingWindow();
            mView.DataContext = mViewModel;
            return mView;
        }
    }
}
