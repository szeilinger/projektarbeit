﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomerAddController.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the CustomerAddController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using Framework.Models;
using Framework.Various;
using Framework.ViewModels;
using Framework.Views;
using NHibernate.Criterion;

namespace Framework.Controllers
{
    class CustomerAddController
    {
        private CustomerAddWindow mView;
        private CustomerAddViewModel mViewModel;

        public Customer AddCustomer()
        {
            mView = new CustomerAddWindow();
            var customer = new Customer();
            int nextNumber = Convert.ToInt32(GetMax());
            customer.CustomerNumber = (nextNumber + 1).ToString();
            mViewModel = new CustomerAddViewModel()
            {
                Model = customer,
                OkCommand = new RelayCommand(ExecuteOkCommand, (o) => mViewModel.IsValid),
                CancelCommand = new RelayCommand(ExecuteCancelCommand),
                AddCityCommand = new RelayCommand(ExecuteAddCityCommand)
            };

            mView.DataContext = mViewModel;
            mView.Title = "Kunden anlegen";

            return mView.ShowDialog() == true ? mViewModel.Model : null;
        }

        private void ExecuteAddCityCommand(object obj)
        {
            CityAddController c = new CityAddController();
            c.EditCity(mViewModel.Model.Address.City);
        }

        private string GetMax()
        {
            var session = HybridSessionBuilder.OpenSession();
            var returnList = session.CreateCriteria<Customer>().SetProjection(Projections.Max<Customer>(x => x.CustomerNumber)).UniqueResult();
            return (string)returnList;
        }

        private void ExecuteOkCommand(object obj)
        {
            mView.DialogResult = true;
            mView.Close();
        }

        private void ExecuteCancelCommand(object obj)
        {
            mView.DialogResult = false;
            mView.Close();
        }

        internal Customer EditCustomer(Customer cust)
        {
            mView = new CustomerAddWindow();
            mViewModel = new CustomerAddViewModel()
            {
                Model = cust,
                OkCommand = new RelayCommand(ExecuteOkCommand, (o) => mViewModel.IsValid),
                CancelCommand = new RelayCommand(ExecuteCancelCommand),
                AddCityCommand = new RelayCommand(ExecuteAddCityCommand)
            };

            mView.DataContext = mViewModel;
            mView.Title = "Kunden bearbeiten";

            return mView.ShowDialog() == true ? mViewModel.Model : null;
        }
    }
}
