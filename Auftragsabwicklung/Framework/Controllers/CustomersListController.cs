﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CustomersListController.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the CustomersListController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Framework.Models;
using Framework.Various;
using Framework.ViewModels;
using Framework.Views;

namespace Framework.Controllers
{
    class CustomersListController : IController
    {
        private Repository<Customer> mCustomerRepository;
        private CustomerListViewModel mViewModel;

        public Control Initialize()
        {
            mCustomerRepository = new Repository<Customer>();
            
            var view = new CustomersListWindow();
            mViewModel = new CustomerListViewModel
            {
                Models = new ObservableCollection<Customer>(mCustomerRepository.GetAll()),
                AddCommand = new RelayCommand(AddCommandExecute),
                EditCommand = new RelayCommand(EditCommandExecute, DeleteCommandCanExecute),
                DeleteCommand = new RelayCommand(DeleteCommandExecute, DeleteCommandCanExecute)
            };

            view.DataContext = mViewModel;
            return view;
        }

        private void AddCommandExecute(object obj)
        {
            var addedObject = new CustomerAddController().AddCustomer();
            if (addedObject != null)
            {
                mCustomerRepository.Save(addedObject);
                mViewModel.Models.Add(addedObject);
            }
        }

        private void EditCommandExecute(object obj)
        {
            var editedObject = new CustomerAddController().EditCustomer(mViewModel.SelectedModel);
            if (editedObject != null)
            {
                mCustomerRepository.Update(editedObject);
                int selected = mViewModel.Models.IndexOf(editedObject);
                mViewModel.Models = new ObservableCollection<Customer>(mCustomerRepository.GetAll());
                mViewModel.SelectedModel = mViewModel.Models.ElementAt(selected);
            }
            HybridSessionBuilder.OpenSession().Refresh(mViewModel.SelectedModel);
        }

        private void DeleteCommandExecute(object obj)
        {
            if (mViewModel.SelectedModel != null)
            {
                var result = MessageBox.Show("Möchten Sie diesen Kunden " + mViewModel.SelectedModel.CustomerNumber + " wirklich löschen?", "Warnung", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.No)
                {
                    return;
                }
                try
                {
                    mCustomerRepository.Delete(mViewModel.SelectedModel);
                    mViewModel.Models.Remove(mViewModel.SelectedModel);
                }
                catch (Exception)
                {
                    MessageBox.Show("Dieser Kunde kann nicht gelöscht werden!\nBitte löschen Sie erst den Auftrag!");
                }
            }
        }
        private bool DeleteCommandCanExecute(object obj)
        {
            return mViewModel.SelectedModel != null;
        }
    }
}