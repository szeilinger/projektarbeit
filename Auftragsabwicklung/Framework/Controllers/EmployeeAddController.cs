﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EmployeeAddController.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the EmployeeAddController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Framework.Models;
using Framework.Various;
using Framework.ViewModels;
using Framework.Views;
using NHibernate.Criterion;

namespace Framework.Controllers
{
    internal class EmployeeAddController
    {
        private EmployeeAddWindow mView;
        private EmployeeAddViewModel mViewModel;

        public Employee AddEmployee()
        {
            mView = new EmployeeAddWindow();
            var employee = new Employee();
            int nextNumber = GetMax();
            employee.EmployeeNumber = nextNumber + 1;
            mViewModel = new EmployeeAddViewModel()
            {
                Model = employee,
                OkCommand = new RelayCommand(ExecuteOkCommand, (o) => mViewModel.IsValid),
                CancelCommand = new RelayCommand(ExecuteCancelCommand),
                AddCityCommand = new RelayCommand(ExecuteAddCityCommand)
            };

            mView.DataContext = mViewModel;
            mView.Title = "Mitarbeiter anlegen";

            return mView.ShowDialog() == true ? mViewModel.Model : null;
        }

        private void ExecuteAddCityCommand(object obj)
        {
            CityAddController c = new CityAddController();
            if (mViewModel.Model.Address.City != null)
            {
                c.EditCity(mViewModel.Model.Address.City);
            }
            else
            {
                mViewModel.Model.Address.City = c.AddCity();
            }
        }

        private int GetMax()
        {
            var session = HybridSessionBuilder.OpenSession();
            var returnList = session.CreateCriteria<Employee>().SetProjection(Projections.Max<Employee>(x => x.EmployeeNumber)).UniqueResult();
            return (int)returnList;
        }

        private void ExecuteOkCommand(object obj)
        {
            mView.DialogResult = true;
            mView.Close();
        }

        private void ExecuteCancelCommand(object obj)
        {
            mView.DialogResult = false;
            mView.Close();
        }

        internal Employee EditEmployee(Employee employee)
        {
            mView = new EmployeeAddWindow();
            mViewModel = new EmployeeAddViewModel()
            {
                Model = employee,
                OkCommand = new RelayCommand(ExecuteOkCommand, (o) => mViewModel.IsValid),
                CancelCommand = new RelayCommand(ExecuteCancelCommand),
                AddCityCommand = new RelayCommand(ExecuteAddCityCommand)
            };

            mView.DataContext = mViewModel;
            mView.Title = "Mitarbeiter bearbeiten";

            return mView.ShowDialog() == true ? mViewModel.Model : null;
        }
    }
}
