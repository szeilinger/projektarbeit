﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CountryAddController.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the CountryAddController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using Framework.Models;
using Framework.Various;
using Framework.ViewModels;
using Framework.Views;

namespace Framework.Controllers
{
    class CountryAddController
    {
        private CountryAddWindow mView;
        private CountryAddViewModel mViewModel;

        public Country AddCountry()
        {
            mView = new CountryAddWindow();
            mViewModel = new CountryAddViewModel()
            {
                Model = new Country(),
                OkCommand = new RelayCommand(ExecuteOkCommand, (o) => mViewModel.IsValid),
                CancelCommand = new RelayCommand(ExecuteCancelCommand)
            };

            mView.DataContext = mViewModel;
            mView.Title = "Land anlegen";

            return mView.ShowDialog() == true ? mViewModel.Model : null;
        }

        private void ExecuteOkCommand(object obj)
        {
            mView.DialogResult = true;
            mView.Close();
        }

        private void ExecuteCancelCommand(object obj)
        {
            mView.DialogResult = false;
            mView.Close();
        }

        internal Country EditCountry(Country country)
        {
            mView = new CountryAddWindow();
            mViewModel = new CountryAddViewModel()
            {
                Model = country,
                OkCommand = new RelayCommand(ExecuteOkCommand, (o) => mViewModel.IsValid),
                CancelCommand = new RelayCommand(ExecuteCancelCommand)
            };

            mView.DataContext = mViewModel;
            mView.Title = "Land bearbeiten";

            return mView.ShowDialog() == true ? mViewModel.Model : null;
        }
    }
}
