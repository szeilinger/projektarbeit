﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Framework.Models;
using Framework.Various;
using Framework.ViewModels;
using Framework.Views;
using Framework.Views.Controls;
using Microsoft.Win32;

namespace Framework.Controllers
{
    class MainController : IController
    {
        private MainWindow mView;
        private MainViewModel mViewModel;

        public Control Initialize()
        {
            mView = new MainWindow();
            mViewModel = new MainViewModel
                             {
                                 AboutCommand = new RelayCommand(AboutCommandExecute),
                                 ChooseDatabaseCommand = new RelayCommand(ChooseDatabaseCommandExecute),
                                 CloseCommand = new RelayCommand(CloseCommandExecute),
                                 MainWindowCommand = new RelayCommand(MainWindowCommandExecute)
                             };
            mView.DataContext = mViewModel;

            mView.ShowDialog();

            return mView;
        }

        private void MainWindowCommandExecute(object obj)
        {
            if (!(obj is string)) return;

            var title = (string)obj;

            mViewModel.ClearDetails();
            var l = new Label { FontWeight = FontWeights.ExtraBold, Margin = new Thickness(5), Content = title, FontSize = 15 };
            mViewModel.AddDetail(l);

            switch (title)
            {
                case "Aufträge": CreateButtons(mViewModel.OrderDetails); break;
                case "Reporting": CreateButtons(mViewModel.ReportingDetails); break;
                default: CreateButtons(mViewModel.MasterDetails); break;
            }
        }

        private void CreateButtons(IEnumerable<StartItem> details)
        {
            var buttons = from detail in details
                          where detail.Controller != null
                          select new Button
                          {
                              Content = detail.Caption,
                              Margin = new Thickness(10),
                              Command = new OpenWindowCommand(detail.Controller, ControllerButtonExecute)
                          };
            foreach (var b in buttons)
            {
                mViewModel.AddDetail(b);
            }
        }

        private void ControllerButtonExecute(object obj, IController controller)
        {
            // Secruity prompt for more than three elements
            if (mViewModel.StackPanel.Children.Count > 2)
            {
                var result = MessageBox.Show("Möchten Sie wirklich eine weitere Liste öffnen?", "Warnung", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.No)
                {
                    OpenWindowCommand.InstanciatedControllers.Remove(controller.GetType());
                    return;
                }
            }

            // StackPanel which contains the Window and a close-Button
            var childStackPanel = new Grid { Margin = new Thickness(10, 5, 10, 5) };
            var panel = controller.Initialize();
            var closeButton = new CloseButton();
            childStackPanel.Children.Add(closeButton);
            childStackPanel.Children.Add(panel);

            Grid.SetZIndex(closeButton, 1);
            closeButton.Click += (sender, args) =>
            {
                OpenWindowCommand.InstanciatedControllers.Remove(controller.GetType());
                mViewModel.StackPanel.Children.Remove(childStackPanel);
            };

            mViewModel.StackPanel.Children.Add(childStackPanel);
        }

        #region Menu
        private void AboutCommandExecute(object obj)
        {
            MessageBox.Show("Auftragsverwaltung Tomate\n\nCopyright 2012 by Simone Zeilinger & Jan Bader", "About",
                            MessageBoxButton.OK, MessageBoxImage.Information, MessageBoxResult.OK);
        }

        private void ChooseDatabaseCommandExecute(object obj)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            var result = openFile.ShowDialog();
            if (result.Value)
            {
                HybridSessionBuilder.ChangeDatabase(openFile.FileName);
            }
        }

        private void CloseCommandExecute(object obj)
        {
            mView.Close();
        }
        #endregion
    }
}
