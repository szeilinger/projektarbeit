﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="OrderAddController.cs" company="DHBW Stuttgart Campus Horb">
// Authors: Jan Bader & Simone Magdalena Zeilinger
// </copyright>
// <summary>
//   Defines the OrderAddController type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.ObjectModel;
using System.Linq;
using Framework.Various;
using Framework.ViewModels;
using Framework.Views;
using NHibernate.Criterion;
using Order = Framework.Models.Order;
using OrderLine = Framework.Models.OrderLine;

namespace Framework.Controllers
{
    class OrderAddController
    {
        private OrderAddWindow mView;
        private Repository<OrderLine> mOrderLineRepository;
        private OrderAddViewModel mViewModel;

        public Order AddOrder()
        {
            mView = new OrderAddWindow();
            CreateViewModel(null);

            mView.DataContext = mViewModel;
            mView.Title = "Auftrag anlegen";

            return mView.ShowDialog() == true ? mViewModel.Model : null;
        }

        private void CreateViewModel(Order order)
        {
            mOrderLineRepository = new Repository<OrderLine>();
            mViewModel = new OrderAddViewModel()
            {
                OkCommand = new RelayCommand(ExecuteOkCommand, (o) => mViewModel.IsValid),
                CancelCommand = new RelayCommand(ExecuteCancelCommand),
                AddCommand = new RelayCommand(AddCommandExecute),
                EditCommand = new RelayCommand(EditCommandExecute, DeleteCommandCanExecute),
                DeleteCommand = new RelayCommand(DeleteCommandExecute, DeleteCommandCanExecute)
            };

            if (order == null)
            {
                mViewModel.Model = new Order();
                string orderNumber = GetMax();
                string[] strings = orderNumber.Split('-');
                strings[1] = (Convert.ToInt32(strings[1]) + 1).ToString();
                mViewModel.Model.OrderNumber = string.Join("-", strings);
                mViewModel.Models = new ObservableCollection<OrderLine>();
            }
            else
            {
                mViewModel.Model = order;
                mViewModel.Models = new ObservableCollection<OrderLine>(order.OrderLines);
            }
        }

        private string GetMax()
        {
            var session = HybridSessionBuilder.OpenSession();
            var returnList = session.CreateCriteria<Order>().SetProjection(Projections.Max<Order>(x => x.OrderNumber)).UniqueResult();
            return (string)returnList;
        }

        private void ExecuteOkCommand(object obj)
        {
            mView.DialogResult = true;
            mView.Close();
        }

        private void ExecuteCancelCommand(object obj)
        {
            mView.DialogResult = false;
            mView.Close();
        }

        private void AddCommandExecute(object obj)
        {
            new Repository<Order>().Save(mViewModel.Model);
            var addedObject = new OrderLineAddController().AddOrderLine(mViewModel.Model);
            if (addedObject != null)
            {
                mOrderLineRepository.Save(addedObject);
                mViewModel.Models.Add(addedObject);
            }
        }

        private void EditCommandExecute(object obj)
        {
            var editedObject = new OrderLineAddController().EditOrderLine(mViewModel.SelectedModel);
            if (editedObject != null)
            {
                mOrderLineRepository.Update(editedObject);
                int selected = mViewModel.Models.IndexOf(editedObject);
                mViewModel.Models =
                    new ObservableCollection<OrderLine>(
                        mOrderLineRepository.Query().Where(x => x.Order == mViewModel.Model).List<OrderLine>());
                mViewModel.SelectedModel = mViewModel.Models.ElementAt(selected);
            }
            HybridSessionBuilder.OpenSession().Refresh(mViewModel.SelectedModel);
        }

        private void DeleteCommandExecute(object obj)
        {
            if (mViewModel.SelectedModel != null)
            {
                mViewModel.Model.OrderLines.Remove(mViewModel.SelectedModel);
                mOrderLineRepository.Delete(mViewModel.SelectedModel);
                mViewModel.Models.Remove(mViewModel.SelectedModel);
            }
        }
        private bool DeleteCommandCanExecute(object obj)
        {
            return mViewModel.SelectedModel != null;
        }

        internal Order EditOrder(Order order)
        {
            mView = new OrderAddWindow();
            CreateViewModel(order);

            mView.DataContext = mViewModel;
            mView.Title = "Auftrag bearbeiten";

            return mView.ShowDialog() == true ? mViewModel.Model : null;
        }
    }
}