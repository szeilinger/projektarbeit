﻿using System.Windows;
using System.Windows.Threading;
using Framework.Controllers;
using Framework.Various;
using NHibernate;
using NHibernate.Exceptions;

namespace Framework
{
    /// <summary>
    /// Interaktionslogik für "App.xaml"
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            DispatcherUnhandledException += UnhandledException;
            var x = new MainController().Initialize();
        }

        void UnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            if (e.Exception is GenericADOException)
            {
                MessageBox.Show("Fehler beim Datenbankzugriff. Bitte versuchen Sie es erneut.", "Datenbankfehler",
                                MessageBoxButton.OK, MessageBoxImage.Error);
                HybridSessionBuilder.ResetSession();
            }
            if (e.Exception is PropertyValueException)
            {
                MessageBox.Show("Stellen Sie sicher, dass alle Felder korrekt ausgefüllt sind.", "Ungültige Eingabe",
                                MessageBoxButton.OK, MessageBoxImage.Error);
                HybridSessionBuilder.ResetSession();
            }
        }
    }
}
